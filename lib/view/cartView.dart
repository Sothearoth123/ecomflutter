import 'package:flutter/material.dart';

class CartView extends StatelessWidget {
  final String name;
  final String image;
  final String price;

  const CartView(
      {Key? key, required this.name, required this.image, required this.price})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SingleChildScrollView(
        child: ListView.builder(
            shrinkWrap: true,
            itemCount: 1,
            itemBuilder: (context, index) {
              // for remove item
              return Dismissible(key: UniqueKey(),
               direction: DismissDirection.endToStart,
              onDismissed: (_){
                
              },
              background: Container(
                  color: Colors.red,
                  margin: EdgeInsets.symmetric(horizontal: 15),
                  alignment: Alignment.centerRight,
                  child: Icon(
                    Icons.delete,
                    color: Colors.white,
                  ),
                ),
               child:Container(
                    margin: EdgeInsets.fromLTRB(20, 5, 20, 0),
                    height: 100,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        boxShadow: [
                          BoxShadow(
                              blurRadius: 10,
                              offset: Offset(4, 4),
                              color: Colors.grey.withOpacity(0.4))
                        ]),
                    child: Row(
                      children: [
                        Container(
                          width: 70,
                          height: 70,
                          child: Image.network(image),
                        ),
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          // mainAxisAlignment: MainAxisAlignment.start,
                          // crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Container(
                              // margin: EdgeInsets.fromLTRB(20, 5, 20, 5),
                              child: Text(name),
                            ),
                            Container(
                              //margin: EdgeInsets.fromLTRB(20, 5, 20, 5),
                              child: Text("\$ ${price}"),
                            ),
                            Container(
                              margin: EdgeInsets.fromLTRB(20, 5, 20, 5),
                              child: Text(
                                "Size: M | Color: Grey",
                                style: TextStyle(color: Colors.grey),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.15,
                        ),
                        Container(
                          alignment: Alignment.center,
                          width: 60,
                          height: 40,
                          decoration: BoxDecoration(
                              color: Colors.grey[100],
                              borderRadius: BorderRadius.all(Radius.circular(5))),
                          child: Text("1"),
                        )
                      ],
                    ),
                  )
                );
                  //);
            }),
      ),
    );
  }
}


 