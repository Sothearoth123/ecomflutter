import 'dart:convert';
//import 'dart:html';
import 'package:http/http.dart' as http;
import 'package:my_testflutter/model/collectionModel.dart';
import 'package:my_testflutter/model/itemModel.dart';
import 'package:my_testflutter/model/mainModel.dart';

class MainService {
  // Future<List<MainModel>> getService() async {
  //   var get = await http.get(Uri.parse("http://127.0.0.1/stories/all"));
  //   print(" ==> ${get.body}");
  //   return ListModel.fromJson(json.decode(get.body)).listModel!;
  // }

  // Future<DetailModel> getDetailStoryService(String id) async {
  //   var get = await http.get(Uri.parse("http://127.0.0.1/stories/detail/$id"));
  //   return DetailModel.fromJson(json.decode(get.body));
  // }

  Future<List<ListCateItemModel>> getItemCateService() async {
    var get = await http
        .get(Uri.parse("http://localhost:8888/readDB4Flutter/index.php"));
    print("CateItem ==> ${get.body}");
    //return ListCateItemModel.fromJson(json.decode(get.body));
    return CateItemModel.fromJson(json.decode(get.body)).categoriesModel!;
  }

  Future<List<ListWomanItemModel>> getItemWomanService() async {
    var get = await http
        .get(Uri.parse("http://localhost:8888/readDB4Flutter/woman.php"));
    print("Woman ==> ${get.body}");

    return WomanItemModel.fromJosn(json.decode(get.body)).womanItemModel!;
  }

  // Future<ResultModel> getCollectionService() async {
  //   var get = await http.get(Uri.parse(
  //       "http://phartepnimet.nexvissolution.com/api/services/app/EComFeature/GetAllSaleFeature?MaxResultCount=4"));
  //   print("Collection ==> ${get.body}");

  //   //return ResultModel.fromJson(json.decode(get.body)).itemListModel!;
  //   return ResultModel.fromJson(json.decode(get.body));
  // }
}
