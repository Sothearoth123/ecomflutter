import 'package:flutter/material.dart';
import 'package:my_testflutter/Controller/mainScreen/myApp.dart';

class LoginController extends StatefulWidget {
  const LoginController({Key? key}) : super(key: key);

  @override
  _LoginControllerState createState() => _LoginControllerState();
}

class _LoginControllerState extends State<LoginController> {
  final _key = GlobalKey<FormState>();
  var email = "roth@email.com";
  Widget buildHeader(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(25, 70, 0, 0),
      width: MediaQuery.of(context).size.width * 0.1,
      child: Text(
        'Create your account',
        style: TextStyle(color: Colors.black, fontSize: 40),
      ),
    );
  }

  Widget builtEmailTextField() {
    return Container(
      margin: EdgeInsets.fromLTRB(25, 20, 25, 0),
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(hintText: 'Email', labelText: 'Email'),
        validator: (value) {
          // if (value!.isEmpty) {
          //   return "Please enter email";
          // } else if (value != email) {
          //   return "Wrong email";
          // }
        },
      ),
    );
  }

  Widget builtPWTextField() {
    return Container(
      margin: EdgeInsets.fromLTRB(25, 20, 25, 0),
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        decoration:
            InputDecoration(hintText: 'Password', labelText: 'Password'),
        autofocus: false,
        obscureText: true,
      ),
    );
  }

  Widget buildLoginButton() {
    return Container(
      margin: EdgeInsets.fromLTRB(27, 20, 27, 0),
      height: 40,
      width: double.infinity,
      child: ElevatedButton(
        onPressed: () {
         // if (_key.currentState!.validate()) {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => MyApp()));
         // }
        },
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(Colors.black)),
        child: Text(
          'Login',
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        primary: false,
        children: [
          buildHeader(
            context,
          ),
          SizedBox(
            height: 20,
          ),
          Form(
              key: _key,
              child: Column(
                children: [
                  builtEmailTextField(),
                  SizedBox(
                    height: 8,
                  ),
                  builtPWTextField(),
                  buildLoginButton()
                ],
              )),
        ],
      ),
    );
  }
}
