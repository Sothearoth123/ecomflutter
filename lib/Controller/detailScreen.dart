import 'package:flutter/material.dart';
import 'package:my_testflutter/Controller/cart/cartViewController.dart';

class DetailScreen extends StatelessWidget
    implements CartViewControllerProtocol {
  final String image;
  final String name;
  final String price;

  DetailScreen(
      {Key? key, required this.image, required this.name, required this.price})
      : super(key: key);
  Widget buildBackButton(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 10),
      alignment: Alignment.topLeft,
      child: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back_ios)),
    );
  }

  Widget buildShareIcon() {
    return Container(
      margin: EdgeInsets.only(right: 10),
      alignment: Alignment.topRight,
      child: IconButton(
          onPressed: () {
            // Navigator.pop(context);
          },
          icon: Icon(Icons.share)),
    );
  }

  Widget buildDetail() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          margin: EdgeInsets.only(left: 25),
          child: Text(
            name,
            style: TextStyle(
                fontSize: 20, color: Colors.black, fontWeight: FontWeight.bold),
          ),
        ),
        Container(
          margin: EdgeInsets.only(right: 25),
          child: Text(
            "\$ ${price}",
            style: TextStyle(
                fontSize: 20, color: Colors.black, fontWeight: FontWeight.bold),
          ),
        )
      ],
    );
  }

  Widget buildImage(BuildContext context) {
    return Stack(children: [
      Container(
        alignment: Alignment.center,
        child: Image.network(
          image,
          height: MediaQuery.of(context).size.height * 0.5,
        ),
      ),
    ]);
  }

  Widget buildRating() {
    return Container(
      margin: EdgeInsets.only(left: 25),
      child: Row(
        children: [
          Icon(
            Icons.star,
            color: Colors.yellow,
            size: 12,
          ),
          Icon(
            Icons.star,
            color: Colors.yellow,
            size: 12,
          ),
          Icon(
            Icons.star,
            color: Colors.yellow,
            size: 12,
          ),
          Icon(
            Icons.star,
            color: Colors.grey,
            size: 12,
          ),
          Icon(
            Icons.star,
            color: Colors.grey,
            size: 12,
          )
        ],
      ),
    );
  }

  Widget buildColorButton() {
    return Container(
      margin: EdgeInsets.fromLTRB(25, 10, 0, 0),
      child: Row(
        children: [
          Container(
            width: 15,
            height: 15,
            decoration: BoxDecoration(
                color: Colors.grey,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.7),
                      offset: Offset(4, 4),
                      blurRadius: 4)
                ]),
          ),
          Container(
            margin: EdgeInsets.only(left: 15),
            width: 15,
            height: 15,
            decoration: BoxDecoration(
                color: Colors.brown,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.7),
                      offset: Offset(4, 4),
                      blurRadius: 4)
                ]),
          ),
          Container(
            margin: EdgeInsets.only(left: 15),
            width: 15,
            height: 15,
            decoration: BoxDecoration(
                color: Colors.black,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.7),
                      offset: Offset(4, 4),
                      blurRadius: 4)
                ]),
          ),
          Container(
            margin: EdgeInsets.only(left: 30),
            child: Text(
              'S',
              style: TextStyle(color: Colors.grey),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 30),
            child: Text(
              'L',
              style: TextStyle(color: Colors.grey),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 30),
            child: Text(
              'XL',
              style: TextStyle(color: Colors.grey),
            ),
          )
        ],
      ),
    );
  }

  Widget buildTextColor() {
    return Container(
      margin: EdgeInsets.fromLTRB(25, 15, 0, 0),
      child: Row(
        children: [
          Text(
            'Color',
            style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
          ),
          Container(
            margin: EdgeInsets.only(left: 75),
            child: Text(
              'Size',
              style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
            ),
          )
        ],
      ),
    );
  }

  Widget buildDescription(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width * 0.8,
        margin: EdgeInsets.fromLTRB(25, 5, 0, 0),
        child: Text(
            "This is new collection just arrive.Good produt This is new collection just arrive.Good produt "));
  }

  Widget alertDialog(BuildContext context) {
    return AlertDialog(
      title: const Text('Add to cart'),
      content: const Text('Do you want add to cart?'),
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.pop(context, 'Cancel'),
          child: const Text('Cancel'),
        ),
        TextButton(
          onPressed: () {
            Navigator.pop(context);

            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => CartViewController(
                        name: name,
                        image: image,
                        price: price,
                        protocol: this)));
          },
          child: const Text('OK'),
        ),
      ],
    );
  }

  Widget buildCartButton(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: Container(
            margin: EdgeInsets.fromLTRB(25, 50, 0, 0),
            height: 40,
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.black),
              ),
              onPressed: () => showDialog<String>(
                  context: context,
                  builder: (BuildContext context) => alertDialog(context)),
              child: const Text(
                'Add To Cart',
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        ),
        SizedBox(width: 40),
        Container(
            margin: EdgeInsets.fromLTRB(0, 50, 25, 0),
            alignment: Alignment.bottomRight,
            child: Card(
              color: Colors.grey.withOpacity(0.4),
              //elevation: elevation,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(32)),
              child: InkWell(
                onTap: () {},
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.favorite,
                    color: Colors.white,
                  ),
                ),
              ),
            ))
      ],
    );
  }

  final appBar = AppBar(
    iconTheme: IconThemeData(
      color: Colors.black, //change your color here
    ),
    backgroundColor: Colors.white,
    elevation: 0,
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: appBar,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: ListView(
                primary: false,
                children: [
                  buildImage(context),
                  buildDetail(),
                  SizedBox(
                    height: 10,
                  ),
                  buildRating(),
                  buildTextColor(),
                  buildColorButton(),
                  buildDescription(context),
                ],
              ),
            ),
            Container(
                margin: EdgeInsets.only(bottom: 25),
                child: buildCartButton(context)),
          ],
        ));
  }

  @override
  getString(String a) {
    var test;
    test = a;
    print("test ==> ${test}");
  }
}
