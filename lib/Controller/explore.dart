import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_testflutter/Controller/cart/cartViewController.dart';
import 'package:my_testflutter/Controller/service/mainService.dart';
import 'package:my_testflutter/category/exploreCollection.dart';
//import 'package:my_testflutter/model/categoryModel.dart';
import 'package:my_testflutter/model/itemModel.dart';

class Explore extends StatefulWidget {
  @override
  _ExploreState createState() => _ExploreState();
}

class _ExploreState extends State<Explore> {
  MainService mainService = MainService();
  List<ListCateItemModel> listCate =
      List<ListCateItemModel>.empty(growable: true);

  void getExploreService() async {
    var respone;
    respone = await mainService.getItemCateService();
    setState(() {
      listCate = respone;
    });
  }

  @override
  void initState() {
    getExploreService();
    super.initState();
  }

  final builtIconCart = AppBar(
    backgroundColor: Colors.grey[50],
    elevation: 0,
    leading: Container(),
    actions: [
      Container(
        margin: EdgeInsets.fromLTRB(0, 10, 10, 0),
        alignment: Alignment.topRight,
        child: IconButton(
            onPressed: () {},
            color: Colors.grey,
            icon: Image.asset(
              "assets/images/cart.png",
              width: 24,
              height: 24,
            )),
      )
    ],
  );

  Widget builtText() {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 10, 0, 0),
      child: Text(
        "Explore",
        style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget builtSearch() {
    return Container(
      margin: EdgeInsets.fromLTRB(16, 15, 16, 0),
      child: Row(
        children: [
          Expanded(
              child: TextField(
            decoration: InputDecoration(
                hintText: "Search",
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                contentPadding: EdgeInsets.all(8.0),
                suffixIcon: Icon(Icons.search)),
          )),
        ],
      ),
    );
  }

  Widget builtImage() {
    return GridView.builder(
        primary: false,
        shrinkWrap: true,
        itemCount: listCate.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 1,
          //  mainAxisSpacing: 8,
        ),
        itemBuilder: (context, index) {
          ListCateItemModel categories = listCate[index];
          return ExploreCollection(
            name: categories.name.toString(),
            image: categories.image.toString(),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: builtIconCart,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          builtText(),
          builtSearch(),
          Expanded(
            child: ListView(
              children: [builtImage()],
            ),
          ),
        ],
      ),
    );
  }
}
