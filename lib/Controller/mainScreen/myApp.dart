
import 'package:flutter/material.dart';
import 'package:my_testflutter/Controller/explore.dart';
import 'package:my_testflutter/Controller/homescreen.dart';
import 'package:my_testflutter/Controller/menu.dart';
import 'package:my_testflutter/Controller/profile.dart';


class MyApp extends StatefulWidget {
  
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late List<Widget> _pageList;
  
  HomeScreen _homeScreen = HomeScreen();
  Explore _explore = Explore();
  Profile _profile = Profile();
  Menu _menu = Menu();

  @override
  void initState() {
    super.initState();
    _pageList = [_homeScreen, _explore, _menu, _profile];
  }

  get _appBar {
    return AppBar(
      //  title: title,
      backgroundColor: Colors.purple,
      actions: <Widget>[
        IconButton(onPressed: () {}, icon: Icon(Icons.settings))
      ],
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(20.0),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "Hello world ",
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
    );
  }

  PageController _scroller = PageController();
  int _currentIndex = 0;
  get _body {
    return Container(
      alignment: Alignment.center,
      child: PageView(
        controller: _scroller,
        physics: NeverScrollableScrollPhysics(),
        children: _pageList,
        onPageChanged: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
      ),
    );
  }

  get _bottomNavigation {
    return BottomNavigationBar(
      selectedItemColor: Colors.black,
      unselectedItemColor: Colors.grey,
      currentIndex: _currentIndex,
      onTap: (index) {
        setState(() {
          _scroller.animateToPage(index,
              duration: Duration(milliseconds: 300), curve: Curves.easeOut);
        });
      },
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(icon: Image.asset("assets/images/shop.png",width: 24,height: 24,),label: "Shop"),
        BottomNavigationBarItem(icon: Image.asset("assets/images/explore.png",width: 24,height: 24,), label: "Explore"),
        BottomNavigationBarItem(icon: Image.asset("assets/images/brand.png",width: 24,height: 24,),
         label: "Brands"),
        BottomNavigationBarItem(icon: Image.asset("assets/images/user.png",width: 24,height: 24,), label: "Profile")
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //  appBar: _appBar,
      bottomNavigationBar: _bottomNavigation,
      body: _body,
    );
  }
}
