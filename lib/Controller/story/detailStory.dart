import 'package:flutter/material.dart';
import 'package:my_testflutter/Controller/service/mainService.dart';
import 'package:my_testflutter/model/mainModel.dart';

class DetailStory extends StatefulWidget {
  final String id;
  final String title;

  const DetailStory({Key? key, required this.id, required this.title})
      : super(key: key);

  @override
  _DetailStoryState createState() => _DetailStoryState();
}

class _DetailStoryState extends State<DetailStory> {
  MainService mainService = MainService();
  String content = "";

  void getDetailService() async {
    DetailModel respone;
   // respone = await mainService.getDetailStoryService(widget.id);
    setState(() {
      //content = respone.content!;
    });
  }

  @override
  void initState() {
    getDetailService();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title, style: TextStyle(color: Colors.black)),
        backgroundColor: Colors.white,
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
          child: Text(
            content,
            style: TextStyle(color: Colors.black),
          ),
        ),
      ),
    );
  }
}
