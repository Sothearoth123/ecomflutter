import 'package:flutter/material.dart';
import 'package:my_testflutter/Controller/service/mainService.dart';
import 'package:my_testflutter/category/category.dart';
import 'package:my_testflutter/category/womenCollection.dart';
import 'package:my_testflutter/model/itemModel.dart';
import 'package:my_testflutter/model/mainModel.dart';
import 'package:my_testflutter/model/womenCollectionModel.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<ListCateItemModel> listCate =
      List<ListCateItemModel>.empty(growable: true);
  List<ListWomanItemModel> listWoman =
      List<ListWomanItemModel>.empty(growable: true);
  MainService mainService = MainService();

  Widget getCollection() {
    return Container(
        height: 230,
        //margin: EdgeInsets.fromLTRB(5, 10, 5, 10),
        margin: EdgeInsets.only(top: 24),
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: listCate.length,
            itemBuilder: (context, index) {
              ListCateItemModel cateModel = listCate[index];
              return Category(
                name: cateModel.name.toString(),
                image: cateModel.image.toString(),
                id: cateModel.id.toString(),
                price: cateModel.price.toString(),
              );
              // return Container(

              //   child: Column(
              //     children: [
              //       Container(
              //         width: 50,
              //         height: 50,
              //         child: Image.network(listStory[index].img.toString())),
              //       Text(listStory[index].titleText.toString())
              //     ],
              //   ),
              // );
            }));
  }

  Widget text() {
    return Container(
      margin: EdgeInsets.all(20),
      child: Text(
        "Designer Collection",
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
      ),
    );
  }

  Widget header() {
    return Stack(
      children: [
        Container(
          child: Image.network(
              "https://assorted.downloads.oppo.com/static/assets/products/oppo-watch-1/images/1920/sec8-img-073222c6333cda52cedac1e01048df38.png"),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
          IconButton(
            icon: Image.asset("assets/images/fav.png",width: 24,height: 24,),
            onPressed: () => {},
          ),
          IconButton(
            icon: Image.asset("assets/images/cart.png",width: 24,height: 24,),
            onPressed: () {},
          ),
          ],
        )
      ],
    );
  }

  Widget getWomenCollection() {
    return GridView.builder(
        primary: false,
        shrinkWrap: true,
        itemCount: listWoman.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 8,
        ),
        itemBuilder: (context, index) {
          ListWomanItemModel womenCollectionModel = listWoman[index];
          return WomenCollection(
              id: womenCollectionModel.id.toString(),
              image: womenCollectionModel.image.toString(),
              name: womenCollectionModel.name.toString(),
              price: womenCollectionModel.price.toString());
        });
  }

  Widget getTextWomen() {
    return Container(
      margin: EdgeInsets.all(20),
      child: Text(
        "Women's Collection",
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
      ),
    );
  }

  void getItemCateService() async {
    var respone;
    respone = await mainService.getItemCateService();
    setState(() {
      listCate = respone;
    });
  }

  void getWomanItemService() async {
    var respone;
    respone = await mainService.getItemWomanService();
    setState(() {
      listWoman = respone;
    });
  }

  // void getCollectionService() async {
  //   var respone;
  //   respone = await mainService.getCollectionService();
  // }

  @override
  void initState() {
    getItemCateService();
    getWomanItemService();
   // getCollectionService();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       body: ListView(
      children: [
        header(),
        SizedBox(
          height: 20,
        ),
        text(),
        getCollection(),
        // collectionView()
        SizedBox(
          height: 20,
        ),
        getTextWomen(),
        SizedBox(
          height: 20,
        ),
        getWomenCollection()
      ],
    ));
  }
}
