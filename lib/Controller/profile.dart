import 'package:flutter/material.dart';
import 'package:my_testflutter/view/customIconButton.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  Widget builtIconTop() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        IconButton(
            onPressed: () {},
            icon: Image.asset(
              "assets/images/fav.png",
              width: 24,
              height: 24,
            )),
        IconButton(
            onPressed: () {},
            icon: Image.asset(
              "assets/images/cart.png",
              width: 24,
              height: 24,
            )),
      ],
    );
  }

  Widget builtProfileImage() {
    return Column(
      children: [
        Container(
          width: 100,
          height: 100,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
                image: AssetImage("assets/images/myProfile.jpeg"),
                fit: BoxFit.cover),
          ),
        ),
        Container(
            margin: EdgeInsets.only(top: 15),
            child: Column(
              children: [
                Text(
                  "Oun Roth",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text("thy.sothearoth@dai-ichilife.com.kh"),
              ],
            )),
           Container(
                  padding: EdgeInsets.fromLTRB(20, 50, 20, 0),
                  child: Material(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5),
                    child: ListView(
                      primary: false,
                      shrinkWrap: true,
                      children: [
                        ListTile(
                            title: Text("My Address"),
                            leading: Image.asset("assets/images/location.png"),
                            trailing: Icon(Icons.navigate_next)),
                        Divider(
                          color: Colors.grey[400],
                        ),
                        ListTile(
                            title: Text("Account"),
                            leading: Image.asset("assets/images/acc.png"),
                            trailing: Icon(Icons.navigate_next))
                      ],
                    ),
                  ),
                ),
            Container(
                  padding: EdgeInsets.fromLTRB(20, 50, 20, 0),
                  height: 330,
                  child: Material(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5),
                    child: ListView(
                      primary: false,
                      shrinkWrap: true,
                      children: [
                        ListTile(
                            title: Text("Notification"),
                            leading:
                                Image.asset("assets/images/notification.png"),
                            trailing: Icon(Icons.navigate_next)),
                        Divider(
                          color: Colors.grey[400],
                        ),
                        ListTile(
                            title: Text("Device"),
                            leading: Image.asset("assets/images/device.png"),
                            trailing: Icon(Icons.navigate_next)),
                        Divider(
                          color: Colors.grey[400],
                        ),
                        ListTile(
                            title: Text("Password"),
                            leading: Image.asset("assets/images/pw.png"),
                            trailing: Icon(Icons.navigate_next)),
                        Divider(
                          color: Colors.grey[400],
                        ),
                        ListTile(
                            title: Text("Language"),
                            leading: Image.asset("assets/images/language.png"),
                            trailing: Icon(Icons.navigate_next))
                      ],
                    ),
                  ),
                )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: appBar,
      body: Container(
        color: Colors.grey[200],
        margin: EdgeInsets.only(top: 10),
        child: ListView(
          primary: false,
          children: [builtIconTop(), builtProfileImage()],
        ),
      ),
    );
  }
}
