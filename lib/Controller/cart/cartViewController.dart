import 'package:flutter/material.dart';
import 'package:my_testflutter/view/cartView.dart';

abstract class CartViewControllerProtocol {
  getString(String a);
}


class CartViewController extends StatefulWidget {
  final String name;
  final String image;
  final String price;

  CartViewControllerProtocol? protocol;

  CartViewController(
      {Key? key,
      required this.name,
      required this.image,
      required this.price,
       this.protocol})
      : super(key: key);
  @override
  _CartViewControllerState createState() => _CartViewControllerState();
}

class _CartViewControllerState extends State<CartViewController> {
  final appBar = AppBar(
    iconTheme: IconThemeData(
      color: Colors.black, //change your color here
    ),
    title: Text(
      'My Bag',
      style: TextStyle(color: Colors.black),
    ),
    backgroundColor: Colors.white,
    elevation: 0,
  );

  Widget buildCartList(BuildContext context) {
    return Column(
      children: [
        CartView(
            name: widget.name, image: widget.image, price: "${widget.price}"),
        // SizedBox(height: 30,),
        Column(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(28, 0, 28, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Total Amount",
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "\$78",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 25,
            ),
            Container(
              margin: EdgeInsets.fromLTRB(20, 0, 20, 45),
              //width: MediaQuery.of(context).size.width * 0.85,
              width: double.infinity,
              height: 40,
              child: ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.black)),
                  onPressed: () => backNavigator(context),
                  child:
                      Text("CONTINUE", style: TextStyle(color: Colors.white))),
            ),
          ],
        ),
      ],
    );
  }

  void backNavigator(BuildContext context) {
    var a = "back";
    widget.protocol!.getString(a);
    Navigator.pop(context);
  }

  showBottomSheet() {
    return showModalBottomSheet(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(20))),
        context: context,
        builder: (context) => buildSheet(context));
  }

  Widget buildSheet(BuildContext context) => Column(
        // mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
            width: MediaQuery.of(context).size.width * 0.5,
            child: Text(
              'Choose Your Payment Method',
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                  margin: EdgeInsets.only(left: 20),
                  child: Text("Total amount")),
              Container(
                  margin: EdgeInsets.only(right: 20),
                  child: Text(
                    '\$210',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  )),
            ],
          ),
          Container(
            margin: EdgeInsets.fromLTRB(20, 60, 20, 0),
            width: double.infinity,
            height: 45,
            child: ElevatedButton(
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Colors.black)),
              child: Text('CREDIT CARD'),
              onPressed: () {},
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(20, 20, 20, 60),
            width: double.infinity,
            height: 45,
            child: ElevatedButton(
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Colors.blue)),
              child: Text('PAYPAL'),
              onPressed: () {},
            ),
          ),
        ],
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar,
      body: buildCartList(context),
    );
  }
}
