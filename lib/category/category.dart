import 'package:flutter/material.dart';
import 'package:my_testflutter/Controller/detailScreen.dart';
import 'package:my_testflutter/Controller/story/detailStory.dart';
import 'package:my_testflutter/view/customIconButton.dart';

class Category extends StatelessWidget {
  final String id;
  final String name;
  final String image;
  final String price;

  const Category(
      {Key? key,
      required this.name,
      required this.image,
      required this.id,
      required this.price})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    DetailScreen(image: image, name: name, price: "${price}")));
      },
      child: Container(
        width: MediaQuery.of(context).size.width / 2,
        margin: EdgeInsets.only(right: 12),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [buildImage(), buildCateInfo()],
        ),
      ),
    );
  }

  Widget buildImage() {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            color: Colors.grey[100],
            boxShadow: [
              BoxShadow(
                  blurRadius: 10,
                  color: Colors.grey.withOpacity(0.4),
                  offset: Offset(10, 10)),
            ],
            borderRadius: BorderRadius.circular(12),
          ),
          margin: EdgeInsets.only(left: 12),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            child: Image.network(
              image,
              height: 120,
              width: double.infinity,
              fit: BoxFit.cover,
            ),
          ),
        ),
        Align(
          alignment: Alignment.bottomRight,
          child: CustomIconButton(
            radius: 32,
            icon: Icon(
              Icons.favorite,
              color: Colors.red,
            ),
            onPressed: () {},
          ),
        )
      ],
    );
    //);
  }

  Widget buildCateInfo() {
    return Container(
      margin: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            name,
            style: TextStyle(
                fontSize: 15, fontWeight: FontWeight.w500, color: Colors.grey),
          ),
          SizedBox(
            height: 4,
          ),
          Text(
            ("\$ $price.00"),
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }
}
