import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_testflutter/Controller/detailScreen.dart';
import 'package:my_testflutter/model/itemModel.dart';
import 'package:my_testflutter/model/womenCollectionModel.dart';

class WomenCollection extends StatelessWidget {
  final String id;
  final String name;
  final String image;
  final String price;

  WomenCollection(
      {required this.id,
      required this.image,
      required this.name,
      required this.price});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DetailScreen(
                      image: image,
                      name: name,
                      price: "${price}",
                    )));
      },
      child: Container(
        margin: EdgeInsets.fromLTRB(12, 12, 12, 0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                blurRadius: 10,
                color: Colors.grey.withOpacity(0.4),
                offset: Offset(4, 4)),
          ],
        ),
        child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return Column(
              children: [
                Image.network(
                  image,
                  height: constraints.maxHeight * 0.70,
                  width: double.infinity,
                  fit: BoxFit.cover,
                ),
                SizedBox(height: 8),
                Text(
                  name,
                  style: TextStyle(color: Colors.black, fontSize: 15),
                ),
                SizedBox(height: 8),
                Text(
                  "\$ ${price}",
                  style: TextStyle(color: Colors.black, fontSize: 15),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
