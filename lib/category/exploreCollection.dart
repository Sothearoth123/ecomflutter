import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:my_testflutter/model/categoryModel.dart';
// import 'package:my_testflutter/model/itemModel.dart';

class ExploreCollection extends StatelessWidget {
  // final ListCateItemModel categories;
  // const ExploreCollection({Key? key, required this.categories})
  //     : super(key: key);
  final String image;
  final String name;

  ExploreCollection({required this.image, required this.name});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10)),
          boxShadow: [
            BoxShadow(
                offset: Offset(4, 4),
                blurRadius: 4,
                color: Colors.grey.withOpacity(0.4))
          ]),
      child: Stack(
        children: [
          Image.network(
            image,
            //  height: 200,
            height: MediaQuery.of(context).size.width,
            width: double.infinity,
            fit: BoxFit.cover,
          ),
          Container(
            alignment: Alignment.center,
            child: Text(
              name,
              style: TextStyle(
                  color: Colors.grey,
                  fontSize: 28,
                  fontWeight: FontWeight.bold),
            ),
          )
        ],
      ),
    );
  }
}
