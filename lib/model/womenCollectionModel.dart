class WomenCollectionModel {
  String name;
  String image;
  double price;

  WomenCollectionModel(
      {required this.name, required this.price, required this.image});
}

final List<WomenCollectionModel> womenCollection = [
  WomenCollectionModel(
      name: "Hand bag",
      price: 25.99,
      image:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRUKmobFq4zVV7pQXNPH35oEB4Z8m5LSWljPg&usqp=CAU"),
  WomenCollectionModel(
      name: "Stilietto heel",
      price: 38.99,
      image:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRUKmobFq4zVV7pQXNPH35oEB4Z8m5LSWljPg&usqp=CAU"),
  WomenCollectionModel(
      name: "Wedding Dress",
      price: 19.99,
      image:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRUKmobFq4zVV7pQXNPH35oEB4Z8m5LSWljPg&usqp=CAU"),
  WomenCollectionModel(
      name: "T-shirt",
      price: 20.55,
      image:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRUKmobFq4zVV7pQXNPH35oEB4Z8m5LSWljPg&usqp=CAU"),
];
