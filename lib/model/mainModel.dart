class ListModel {
  List<MainModel>? listModel;
  ListModel.fromJson( json) {
    listModel = List<MainModel>.empty(growable: true);
    json.forEach((element) {
      listModel!.add(MainModel.fromJson(element));
    });
    // listModel = List<MainModel>.from(
    //         json.map((x) => MainModel.fromJson(x)));
  }
}

class MainModel {
  String? authorText;
  String? id;
  String? titleText;
  String? img;

  MainModel({this.authorText, this.id, this.titleText, this.img});

  MainModel.fromJson(Map<String, dynamic> json) {
    authorText = json['authorText'];
    id = json['id'];
    titleText = json['titleText'];
    img = json['img'];
    
   
  }

}

class DetailModel {
  String? id;
  String? content;
  String? titleText;

  DetailModel({this.id, this.content, this.titleText});

  DetailModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    content = json['content'];
    titleText = json['titleText'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['content'] = this.content;
    data['titleText'] = this.titleText;
    return data;
  }
}