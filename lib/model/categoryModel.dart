//import 'package:flutter/cupertino.dart';

class CatagoryModel {
  String name;
  double price;
  String image;

  CatagoryModel({required this.image, required this.price, required this.name});
}

final List<CatagoryModel> cateList = [
  CatagoryModel(
      image:
          "https://cdn.shopify.com/s/files/1/0057/8938/4802/files/2_a25aff7a-b5c4-4565-a111-6e1ce2d5b5f0.png?v=1624268771",
      price: 49.99,
      name: "Watch"),
  CatagoryModel(
      image:
          "https://cdn.shopify.com/s/files/1/0057/8938/4802/files/2_a25aff7a-b5c4-4565-a111-6e1ce2d5b5f0.png?v=1624268771",
      price: 89.99,
      name: "OnePlus watch"),
  CatagoryModel(
      image:
          "https://cdn.shopify.com/s/files/1/0438/5384/0533/files/ROCKSHELL_HEADER_736x.png?v=1616673460",
      price: 120.19,
      name: "RGM Watch"),
  CatagoryModel(
      image:
          "https://cdn.shopify.com/s/files/1/0057/8938/4802/files/2_a25aff7a-b5c4-4565-a111-6e1ce2d5b5f0.png?v=1624268771",
      price: 50.99,
      name: "Platinum Watch"),
  CatagoryModel(
      image:
          "https://assorted.downloads.oppo.com/static/assets/products/oppo-watch-1/images/1920/sec4-img1-96a8194bea3dddea2bf88db6c727cb48.png",
      price: 99.99,
      name: "RoseFiled Watch")
];
