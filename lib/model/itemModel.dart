
class ListCateItemModel {
  String? id;
  String? name;
  String? price;
  String? image;

  ListCateItemModel({this.id, this.name, this.image, this.price});

  ListCateItemModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    price = json['price'];
    image = json['image'];
  }
}

class CateItemModel {

  List<ListCateItemModel>? categoriesModel;
  CateItemModel({this.categoriesModel});
  CateItemModel.fromJson(Map<String, dynamic> json) {
    var categories = json['categories'] as List;
    print(categories.runtimeType);
    List<ListCateItemModel> categoriesList =
        categories.map((e) => ListCateItemModel.fromJson(e)).toList();
    categoriesModel = categoriesList;
  }
}

class ListWomanItemModel {
  String? id;
  String? name;
  String? image;
  String? price;

  ListWomanItemModel({this.id, this.name, this.image, this.price});

  ListWomanItemModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    price = json['price'];
    image = json['image'];
  }
}

class WomanItemModel {
  List<ListWomanItemModel>? womanItemModel;
  WomanItemModel({this.womanItemModel});

  WomanItemModel.fromJosn(Map<String, dynamic> json) {
    var woman = json['woman'] as List;
    print(woman.runtimeType);
    List<ListWomanItemModel> womanList =
        woman.map((e) => ListWomanItemModel.fromJson(e)).toList();
    womanItemModel = womanList;
  }
}
