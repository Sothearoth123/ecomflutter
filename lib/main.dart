
import 'package:flutter/material.dart';
import 'package:my_testflutter/Controller/login/login.dart';

void main() {
  runApp(MyBottomNavigation());
}

class MyBottomNavigation extends StatefulWidget {
  @override
  _MyBottomNavigationState createState() => _MyBottomNavigationState();
}

class _MyBottomNavigationState extends State<MyBottomNavigation> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: LoginController());
  }
}
